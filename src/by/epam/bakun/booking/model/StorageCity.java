package by.epam.bakun.booking.model;

import by.epam.bakun.booking.model.hotels.Hotel;
import by.epam.bakun.booking.model.hotels.SqlHotel;
import by.epam.bakun.booking.util.DataBaseManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StorageCity {
    private List<City> storageCity;

    private Connection connection;
    private Statement statement;

    public StorageCity() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = DataBaseManager.getProperty("url");
            String user = DataBaseManager.getProperty("user");
            String password = DataBaseManager.getProperty("password");
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<City> getStorageCity() {
        try {
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select from city");
            while (rs.next()) {
                City city = new City();
                city.setCityId(Integer.parseInt(rs.getString("city_id")));
                city.setCountryId(Integer.parseInt(rs.getString("country_country_id")));
                city.setCityName(rs.getString("city_name"));
                storageCity.add(city);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return storageCity;
    }

    public void saveCity(City city) {
        try (PreparedStatement ps = connection.prepareStatement("INSERT INTO city (city_name) VALUES (?,?)")) {
            ps.setString(1, city.getCityName());
            ps.setInt(1, city.getCountryId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public City getCityById(int id) {
        City city = new City();
        try (PreparedStatement ps = connection.prepareStatement("select * from city where city_id=?")) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                city.setCityId(id);
                city.setCityName(rs.getString("city_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return city;
    }

    public City getCityByName(String cityName) {
        City city = new City();
        try (PreparedStatement ps = connection.prepareStatement("select * from city where city_name=?");
                PreparedStatement ps2 = connection.prepareStatement("insert into city (city_name, country_country_id) values (?,?)")) {
            ps.setString(1, cityName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                city.setCityId(rs.getInt("city_id"));
                city.setCityName(cityName);
                return city;
            } else {
                ps2.setString(1, cityName);
                ps2.setInt(2, 1);
                ps2.execute();
                return getCityByName(cityName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return city;
    }






}

